/* 
 Ensure packet header sizes are correct. Idea is to generate
  unreachable code warnings if there is a problem, not actually
  execute this code.
 The standard approach seems to be to use packed structures, but this
  increases code size significantly (a single word write becomes four
  byte writes, etc.). So this code allows us to check if whatever padding
  trick (esp. on EthHeader) has worked.
*/

/* This doesn't seem to do anything... */
/* #pragma GCC diagnostic error "-Wunreachable-code" */
/* SO COMPILE THIS FILE WITH -Wunreachable-code SWITCH passed to gcc */

#include "net.h"

extern int xil_printf(const char*,...);

void sizecheck()
{
  if(sizeof(EthHeader) != 14)
    xil_printf("EthHeader size incorrect.");
  if(sizeof(ARPpkt) - sizeof(EthHeader) != 26)
    xil_printf("ARPpkt size incorrect.");
  if(sizeof(IPpkt) - sizeof(EthHeader) != 20)
    xil_printf("IPpkt size incorrect.");
  if(sizeof(ICMPpkt) - sizeof(IPpkt) != 8)
    xil_printf("ICMPpkt size incorrect.");
  if(sizeof(UDPpkt) - sizeof(IPpkt) != 8)
    xil_printf("UDPpkt size incorrect.");
  if(sizeof(BOOTPpkt) - sizeof(UDPpkt) != 300)
    xil_printf("BOOTPpkt size incorrect.");
}