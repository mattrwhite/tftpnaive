/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* Notes:
 We could reduce code size a little by just using a for loop to delay
  instead of xtmrctr device
*/

#include "xparameters.h"
#include "xtmrctr.h"

XTmrCtr XPS_Timer;

/*
 Alernative approach is to use interrupts: 
   noint = 1; while(noint) { nop() or work() };  interrupt handler: noint = 0;
 Try google code search for "XTmrCtr_SetHandler" for examples
*/

void timerInit (void) {
  XTmrCtr_Initialize(&XPS_Timer, XPAR_XPS_TIMER_1_DEVICE_ID);
  XTmrCtr_SetResetValue(&XPS_Timer, 0, 0x00000000);
}

/* delay is in microseconds */
void usleep (unsigned int delay)
{
  /* convert microseconds to clock ticks; silently set delay to max
    if too large; we assume timer runs at same freq as CPU */
  if(delay > 0xFFFFFFFF / (XPAR_CPU_CORE_CLOCK_FREQ_HZ / 1000000))
    delay = 0xFFFFFFFF;
  else
    delay = delay * (XPAR_CPU_CORE_CLOCK_FREQ_HZ / 1000000);
  
  XTmrCtr_Reset(&XPS_Timer, 0);
  XTmrCtr_Start(&XPS_Timer, 0);
	while (XTmrCtr_GetValue(&XPS_Timer, 0) < delay) {
    /* do nothing ! */
	}
  XTmrCtr_Stop(&XPS_Timer, 0);
}

/* copied from baselib */

/* waiting loop which periodically call a function
 * passed as argument. The loop exits when :
 * - the timeout t is reached (t in ms) or
 * - the int pointed to by f if <> 0
 */
void busyWait (int t, void (*work) (void), int *f)
{
	register int i;

	while (t) {
		for (i = 0; i < 5; ++i) {
			work ();
			if (f)
				if (*f)
					return;
			usleep (200);
		}
		--t;
	}
}
