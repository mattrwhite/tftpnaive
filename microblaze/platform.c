/* #include "arch/cc.h" */
#include "xparameters.h"
#include "xintc.h"
#include "mb_interface.h"
/* #include "xtmrctr_l.h" */


void platform_enable_interrupts()
{
	microblaze_enable_interrupts();
}

void platform_disable_interrupts()
{
	microblaze_disable_interrupts();
}

/* this must be a global !!! */
XIntc intc;

void platform_setup_interrupts()
{
	XIntc* intcp = &intc;

  XIntc_Initialize(intcp, XPAR_XPS_INTC_0_DEVICE_ID);
	XIntc_Start(intcp, XIN_REAL_MODE);

	/* Start the interrupt controller */
	XIntc_MasterEnable(XPAR_XPS_INTC_0_BASEADDR);

	/* Enable timer and EMAC interrupts in the interrupt controller */
	XIntc_EnableIntr(XPAR_XPS_INTC_0_BASEADDR, 
#ifdef TFTPNAIVE_TIMERINTR  
			XPAR_XPS_TIMER_1_INTERRUPT_MASK |
#endif      
		  XPAR_ETHERNET_MAC_IP2INTC_IRPT_MASK);
	microblaze_register_handler((XInterruptHandler)XIntc_InterruptHandler, intcp);
}

int sim_printf(const char * format, ...)
{
  /* what about calculating a one or two byte 
   checksum (as a unique ID) of string and printing that? */
   
  /*
  char mystr[] = "a";
  mystr[0] = format[0];  // print the first character 
  xil_printf(&mystr);
  */
  return 1;
}
