
/*
	Copyright 2001-2004 Georges Menie (www.menie.org)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _BASELIB_H_
#define _BASELIB_H_

#include <stdio.h>
#include <string.h>
#include "printf.h"

#ifndef NULL
#define NULL 0
#endif

/* #define HDL_SIMULATION 1 */
/* use much smaller xil_printf */
/* for simulation, use a printf that won't stall things while waiting for UART */
#ifdef HDL_SIMULATION
extern int sim_printf(const char * format, ...);
#define printf sim_printf
#else
#define printf tfp_printf
#endif

#endif /* _BASELIB_H_ */
