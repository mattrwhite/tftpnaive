
/*
	Copyright 2001-2004 Georges Menie (www.menie.org)

	This file is part of Tftpnaive.

    Tftpnaive is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Tftpnaive is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tftpnaive; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* tftpnaive for Xilinx Microblaze soft CPU ---
 10-23-2008 - started working on tftpnaive interface for Xilinx 
  Ethernet MAC Lite - based on cs89x.c and xemacliteif.c for LWIP
  from Xilinx.  File is named xemaclt.c instead of xemaclite.c to 
  distinguish it from Xilinx's driver code.
 For Dragon Engine/uCdimm boards, main() runs an endless loop until 
  the ethernet controller (CS8900A) asserts LinkOK bit in its link status
  register. Xilinx EMACLite doesn't provide any way to access MII
  management registers, so we will just start sending out packets immediately.
*/

#include "tftpnaive.h"
#include "baselib.h"
#include "net.h"
#include "xparameters.h"
#include "xemaclite.h"
#include "timer.h"
#include "xemaclt.h"
#include "xintc.h"

/* we only support a single device - this is the driver instance data structure */
/*  XEmacLite_Initialize() will take care of zeroing this memory */
XEmacLite xemaclite;

/* Should rename the this due to similarity with driver's send fn name */
int XEmacLtSend (void *pkt, unsigned short length)
{
  if(length > 1514) {
    printf("XEmacLtSend: Packet is too large!");
    return -1;
  }
  
	return (XEmacLite_Send(&xemaclite, pkt, length) == XST_SUCCESS) ? 0 : 1;
}

/* we expect argument to point the driver's instance data structure,
  although this is kind of silly, since we could just use &xemaclite */
void RxEvent (void* xemaclitep)
{
	int len = 0;
	int idxi, idxo;
	/* unsigned short length; */

	idxi = idxiPBuf;
	idxo = idxoPBuf;
	idxi = (++idxi) % MAXPACKETBUF;
  
  XIntc_AckIntr(XPAR_INTC_SINGLE_BASEADDR, 1 << XPAR_XPS_INTC_0_ETHERNET_MAC_IP2INTC_IRPT_INTR);
  
	/* receive the packet */
	if (idxi != idxo) { /* && length < MAXPACKETLEN */
		unsigned char* p = (unsigned char*) &packetBuf[idxi][0];
		len = XEmacLite_Recv((XEmacLite*) xemaclitep, p);
    if(len > MAXPACKETLEN) {
      printf("Received oversized packet - halting due to memory corruption.\r\n");
      while(1) {}
    }
		packetBufLen[idxi] = len;
		idxiPBuf = idxi;
	}
	else {
		/* no more buffers or packet too big, discard new packet */
		XEmacLite_FlushReceive((XEmacLite*) xemaclitep);
		++netif.RxQFull;
	}  
  /* statistics */
  ++netif.TotalIRQ;
  ++netif.TotalEvent;
  ++netif.RxEvent;  
}

void TxEvent (void* xemaclitep)
{
  /* statistics */
  ++netif.TotalIRQ;
  ++netif.TotalEvent;
  ++netif.TxEvent;
}

int XEmacLtSetup (void)
{
	/* XEmacLite_Config *config; */
	XEmacLite* xemaclitep = &xemaclite;

	/* initialize the mac */
	XEmacLite_Initialize(xemaclitep, XPAR_EMACLITE_0_DEVICE_ID);
	xemaclitep->NextRxBufferToUse = 0;

  /* We have to take care of registering the driver's interrupt */
  /*  handler with the interrupt controller (apparently) */
  /* first arg is base address of interrupt controller - driver uses this */
  /*  to lookup the instance data */
  /* second arg is the Interrupt ID for the interrupt controller*/
  /* fourth arg is parameter to pass interrupt handler */
	XIntc_RegisterHandler(XPAR_INTC_SINGLE_BASEADDR, XPAR_XPS_INTC_0_ETHERNET_MAC_IP2INTC_IRPT_INTR,
			(XInterruptHandler)XEmacLite_InterruptHandler, xemaclitep);

	/* set MAC address - called the IEEE Individual Address in tftpnaive */
	XEmacLite_SetMacAddress(xemaclitep, (Xuint8*)(netif.IEEEIA));

	/* flush any frames already received */
	XEmacLite_FlushReceive(xemaclitep);

	/* set Rx, Tx interrupt handlers */
  /* second arg is argument for callback (which is third arg) */
	XEmacLite_SetRecvHandler(xemaclitep, (void *)(xemaclitep), RxEvent);
	XEmacLite_SetSendHandler(xemaclitep, (void *)(xemaclitep), TxEvent);

	/* enable Rx, Tx interrupts */
  XEmacLite_EnableInterrupts(xemaclitep);

  return 0;
}

int XEmacLtReset (void)
{
  /* what else do we need to do here? */
  XEmacLite_DisableInterrupts(&xemaclite);
  return 0;
}
