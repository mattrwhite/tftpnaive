/*
	Copyright 2001-2004 Georges Menie (www.menie.org)

	This file is part of Tftpnaive.

    Tftpnaive is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Tftpnaive is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tftpnaive; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* HW address:
 For now, HW address will be fixed in this software.
 In the future, we may want to read part of the MAC address from DIP switches
 or jumpers, or from the ethernet PHY's 5 bit address.
*/
#ifndef ETH_MAC_ADDR
#define ETH_MAC_ADDR { 0x00, 0x0a, 0x35, 0x00, 0x01, 0x02 }
#endif

#include "tftpnaive.h"
#include "baselib.h"
#include "net.h"
#include "timer.h"
#include "xemaclt.h"

/* int errno; */

int netUp (void)
{
	int error;
  unsigned char hwaddr[] = ETH_MAC_ADDR;

	memcpy (netif.IEEEIA, hwaddr, sizeof(netif.IEEEIA));
	netif.send = XEmacLtSend;

	error = XEmacLtSetup ();
  if (error) {
    printf ("netUp: network interface init failed: code %d\n", error);
    return 1;
  }
	return 0;
}

int netDown (void)
{
	if (XEmacLtReset ()) {
		printf ("netDown: Network interface reset failed\n");
		return 1;
	}
	/* erase IP */
	netif.ip = 0;
	return 0;
}

/* don't bother turning on the Microblaze cache, since we access
 a given byte in SDRAM at most once (to write the boot image) */
 
/* in platform.c */
extern void platform_setup_interrupts(void);
extern void platform_enable_interrupts(void);

int main (void)
{
  /* need interrupts for ethernet */
  platform_setup_interrupts();
  platform_enable_interrupts();
 
	timerInit ();

	if (netUp ())
		return 1;
  
  printf("tftpnaive-mb\n");
	while (1) {
    /* try to get IP address and boot image path */
		netRequestIP ();
    /* try to download boot image */
		if (netBoot () == 0)
			break;
	}
  /* netDown will disable emaclite interrupts, but let's
   also disable all interrupts */
	netDown ();
  platform_disable_interrupts();

  /* jump to entry point of downloaded boot image */
	if (tftp_req && tftp_req->sts && tftp_req->strtf) {
    ((void(*)())tftp_req->strt)();
		/* jump ((void *) tftp_req->strt); */
	}

	return 0;
}
