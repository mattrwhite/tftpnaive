
/*
	Copyright 2001-2004 Georges Menie (www.menie.org)

	This file is part of Tftpnaive.

    Tftpnaive is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Tftpnaive is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tftpnaive; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* History:
 - MRW, 10/25/2008: make spinning progress indicator optional
*/

#include "tftpnaive.h"
#include "net.h"
#include "baselib.h"
#include "timer.h"
#ifdef TFTPNAIVE_CRCCHECK
#include "crc16.h"
#endif

typedef struct {
	UDPpkt udphdr;
	unsigned short opcode;
	unsigned short num;
} TFTPDReplypkt;

/*#define TFTPNAIVE_USEPKTCPY 1*/
#ifdef TFTPNAIVE_USEPKTCPY

#define data_copy pktcpy
static void pktcpy(char *dst, const char *src, unsigned long len)
{
	if (((long)dst) & 1) {
		while (len) {
			*dst++ = *src++;
			--len;
		}
		return;
	}

	while (len >= 4) {
		*((long *)dst) = *((long *)src);
    src += 4;
    dst += 4;
		len -= 4;
	}

	while (len) {
		*dst++ = *src++;
		--len;
	}
}
#else

#define data_copy memcpy

#endif

void processTFTP (TFTPDpkt * tp)
{
	register int nbtry;
	TFTPDReplypkt tftpd_reply_pkt;

	if (!tftp_req)
		return;

	if (tftp_req->port == 0)
		tftp_req->port = tp->udphdr.src_port;

	if (tp->udphdr.iphdr.src_ip == tftp_req->server
		&& tp->udphdr.src_port == tftp_req->port) {
		if (tp->opcode == 3) {
			if (tp->num == tftp_req->bloc || tp->num == tftp_req->bloc - 1) {
				tftpd_reply_pkt.opcode = 4;
				tftpd_reply_pkt.num = tp->num;
				for (nbtry = 4; nbtry; --nbtry) {
					if (sendUDP
						((UDPpkt *) & tftpd_reply_pkt, 4, tftp_req->server,
						 TFTP_LOCAL_PORT, tp->udphdr.src_port) == 0)
						break;
					usleep (1000);
				}
				if (nbtry == 0)
					printf ("TFTP: error sending ACK\n");
				if (tp->num == tftp_req->bloc) {
					register unsigned short len = tp->udphdr.length - 12;

					if (len) {
						data_copy ((void *) (tftp_req->addr + tftp_req->bcnt),
								tp->data, len);
						tftp_req->bcnt += len;
#ifdef TFTPNAIVE_CRCCHECK
						{ int i;
							for(i = 0; i < len; i++)
								tftp_req->crc = (tftp_req->crc<<8) ^ crc16tab[((tftp_req->crc>>8) ^ tp->data[i])&0x00FF];
						}
#endif
					}
#ifdef TFTPNAIVE_SPINNAZ          
          /* draw a spinning indicator - adds over 10KB to .text for microblaze! */
          /* print a "=" or something instead? */
					if ((tftp_req->bloc & 31) == 0) {     
						putchar ("-\\|/"[(tftp_req->bloc>>5) % 4]);
						putchar ('\b');
					}
#endif          
					++tftp_req->bloc;
					if (tp->udphdr.length < 524) {
						tftp_req->sts = 1;
						printf ("TFTP: downloaded %d bytes\n",
								tftp_req->bcnt);
#ifdef TFTPNAIVE_CRCCHECK
						{ unsigned short crc;
							printf ("TFTP: checking crc ...");
							crc = crc16_ccitt((void *)tftp_req->addr, tftp_req->bcnt);
							printf (" %s\n", crc == tftp_req->crc ? "ok" : "error");
						}
#endif
					}
				}
			}
		}
		else if (tp->opcode == 5) {
			printf ("TFTP error %d: %s\n", tp->num, tp->data);
			tftp_req->sts = -tp->num;
		}
	}
}

int sendTFTPRequest (void)
{
	TFTPRpkt tftpr_pkt;
	unsigned short len;

	if (tftp_req) {
		memset (&tftpr_pkt, 0, sizeof tftpr_pkt);
		tftpr_pkt.opcode = 1;
		strcpy (tftpr_pkt.data, tftp_req->file);
		len = strlen (tftp_req->file) + 1;
		strcpy (tftpr_pkt.data + len, "octet");
		len += 8;				/* opcode size + octet\0 size */

		return sendUDP ((UDPpkt *) & tftpr_pkt, len, tftp_req->server,
						TFTP_LOCAL_PORT, TFTP_SRV_PORT);
	}
	return 1;
}
