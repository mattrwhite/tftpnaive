/*
	Copyright 2001-2004 Georges Menie (www.menie.org)

	This file is part of Tftpnaive.

    Tftpnaive is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Tftpnaive is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tftpnaive; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _CS89X_H_
#define _CS89X_H_

#if defined (CONFIG_DRAGEN2)
#define CS89xIOBASE (0x08000040)
#define IRQ_NUM 9
#elif defined (CONFIG_UCDIMM)
#define CS89xIOBASE (0x10000300)
#define IRQ_NUM 20
#define CS89_HW_SWAP
#else
#error "need to configure the cs89 base address, IRQ and swapping configuration here"
#endif

/*-----------*/
/* Registers */
/*-----------*/

/* Bus Interface Registers */
#define CS89xProductIdentificationCode	(0)
#define CS89xIOBaseAddress				(0x20)
#define CS89xInterruptNumber			(0x22)
#define CS89xDMAChannelNumber			(0x24)
#define CS89xDMAStartOfFrame			(0x26)
#define CS89xDMAFrameCount				(0x28)
#define CS89xRxDMAFrameCount			(0x2A)
#define CS89xMemoryBaseAddress			(0x2C)
#define CS89xBootPROMBaseAddress		(0x30)
#define CS89xBootPROMAddressMask		(0x34)
#define CS89xEEPROMCommand				(0x40)
#define CS89xEEPROMData					(0x42)
#define CS89xReceiveFrameByteCounter	(0x50)

/* Status and Control Registers */
#define CS89xReceiverConfiguration		(0x102)
#define CS89xReceiverControl			(0x104)
#define CS89xTransmitConfiguration		(0x106)
#define CS89xTransmitCommandStatus		(0x108)
#define CS89xBufferConfiguration		(0x10A)
#define CS89xLineControl				(0x112)
#define CS89xSelfControl				(0x114)
#define CS89xBusControl					(0x116)
#define CS89xTestControl				(0x118)
#define CS89xInterrupStatusQueue		(0x120)
#define CS89xReceiverEvent				(0x124)
#define CS89xTransmitterEvent			(0x128)
#define CS89xBufferEvent				(0x12C)
#define CS89xReceiverMissCounter		(0x130)
#define CS89xTransmitCollisionCounter	(0x132)
#define CS89xLineStatus					(0x134)
#define CS89xSelfStatus					(0x136)
#define CS89xBusStatus					(0x138)
#define CS89xAUITimeDomainReflectometer	(0x13C)

/* Initiate Transmit Registers */
#define CS89xTransmitCommandRequest		(0x144)
#define CS89xTransmitLength				(0x146)

/* Address Filter Registers */
#define CS89xLogicalAddressFilter		(0x150)
#define CS89xIndividualAddress			(0x158)

/* Receive Frame Location */
#define CS89xReceiveStatus				(0x400)
#define CS89xReceiveLength				(0x402)
#define CS89xReceiveFrame				(0x404)

/* Transmit Frame Location */
#define CS89xTransmitFrame				(0xA00)

/*----------------*/
/* IO access Mode */
/*----------------*/

#define CS89xIOReceiveTransmitDataPort0	((volatile short *)(CS89xIOBASE+0x0))
#define CS89xIOReceiveTransmitDataPort1	((volatile short *)(CS89xIOBASE+0x2))
#define CS89xIOTransmitCommand			((volatile short *)(CS89xIOBASE+0x4))
#define CS89xIOTransmitLength			((volatile short *)(CS89xIOBASE+0x6))
#define CS89xIOInterruptStatusQueue		((volatile const short *)(CS89xIOBASE+0x8))
#define CS89xIOPacketPagePointer		((volatile short *)(CS89xIOBASE+0xA))
#define CS89xIOPacketpageDataPort0		((volatile short *)(CS89xIOBASE+0xC))
#define CS89xIOPacketpageDataPort1		((volatile short *)(CS89xIOBASE+0xE))

/*------------------*/
/* Register content */
/*------------------*/

#define CS89xRxCFGSkip_1		(0x40)
#define CS89xRxCFGStreamE		(0x80)
#define CS89xRxCFGRxOKiE		(0x100)
#define CS89xRxCFGRxDMAonly		(0x200)
#define CS89xRxCFGAutoRxDMAE	(0x400)
#define CS89xRxCFGBufferCRC		(0x800)
#define CS89xRxCFGCRCErroriE	(0x1000)
#define CS89xRxCFGRuntiE		(0x2000)
#define CS89xRxCFGExtraDataiE	(0x4000)

#define CS89xRxCTLIAHashA		(0x40)
#define CS89xRxCTLPromiscuousA	(0x80)
#define CS89xRxCTLRxOKA			(0x100)
#define CS89xRxCTLMulticastA	(0x200)
#define CS89xRxCTLIndividualA	(0x400)
#define CS89xRxCTLBroadcastA	(0x800)
#define CS89xRxCTLCRCErrorA		(0x1000)
#define CS89xRxCTLRuntA			(0x2000)
#define CS89xRxCTLExtraDataA	(0x4000)

#define CS89xTxCFGLossOfCRSiE	(0x40)
#define CS89xTxCFGSQErroriE		(0x80)
#define CS89xTxCFGTxOKiE		(0x100)
#define CS89xTxCFGOutOfWindowiE	(0x200)
#define CS89xTxCFGJaberiE		(0x400)
#define CS89xTxCFGAnycolliE		(0x800)
#define CS89xTxCFG16ColliE		(0x8000)

#define CS89xTxCMDStart5		(0x00)
#define CS89xTxCMDStart381		(0x40)
#define CS89xTxCMDStart1021		(0x80)
#define CS89xTxCMDStartAll		(0xc0)
#define CS89xTxCMDForce			(0x100)
#define CS89xTxCMDOneColl		(0x200)
#define CS89xTxCMDInhibitCRC	(0x1000)
#define CS89xTxCMDTxPadDis		(0x2000)

#define CS89xBufCFGSWintX		(0x40)
#define CS89xBufCFGRxDMAiE		(0x80)
#define CS89xBufCFGRdy4TxiE		(0x100)
#define CS89xBufCFGTxUnderruniE	(0x200)
#define CS89xBufCFGRxMissiE		(0x400)
#define CS89xBufCFGRx128iE		(0x800)
#define CS89xBufCFGTxColOvfloiE	(0x1000)
#define CS89xBufCFGMissOvfloiE	(0x2000)
#define CS89xBufCFGRxDestiE		(0x8000)

#define CS89xLineCTLSerRxOn		(0x40)
#define CS89xLineCTLSerTxOn		(0x80)
#define CS89xLineCTLAUIOnly		(0x100)
#define CS89xLineCTLAutoAUI10BT	(0x200)
#define CS89xLineCTLModBackoffE	(0x800)
#define CS89xLineCTLPolarityDis	(0x1000)
#define CS89xLineCTL2PartDefDis	(0x2000)
#define CS89xLineCTLLoRxSquelch	(0x4000)

#define CS89xSelfCTLRESET		(0x40)
#define CS89xSelfCTLSWSuspend	(0x100)
#define CS89xSelfCTLHWSleepE	(0x200)
#define CS89xSelfCTLHWStandbyE	(0x400)
#define CS89xSelfCTLHC0E		(0x1000)
#define CS89xSelfCTLHC1E		(0x2000)
#define CS89xSelfCTLHCB0		(0x4000)
#define CS89xSelfCTLHCB1		(0x8000)

#define CS89xBusCTLResetRxDMA	(0x40)
#define CS89xBusCTLDMAExtend	(0x100)
#define CS89xBusCTLUseSA		(0x200)
#define CS89xBusCTLMemoryE		(0x400)
#define CS89xBusCTLDMABurst		(0x800)
#define CS89xBusCTLIOCHRDYE		(0x1000)
#define CS89xBusCTLRxDMASize	(0x2000)
#define CS89xBusCTLEnableIRQ	(0x8000)

#define CS89xTestCTLDisableLT	(0x80)
#define CS89xTestCTLENDECLoop	(0x200)
#define CS89xTestCTLAUILoop		(0x400)
#define CS89xTestCTLDisBackoff	(0x800)
#define CS89xTestCTLFDX			(0x4000)

#define CS89xRxEventIAHash		(0x40)
#define CS89xRxEventDribbleBits	(0x80)
#define CS89xRxEventRxOK		(0x100)
#define CS89xRxEventHashed		(0x200)
#define CS89xRxEventIndividual	(0x400)
#define CS89xRxEventBroadcast	(0x800)
#define CS89xRxEventCRCError	(0x1000)
#define CS89xRxEventRunt		(0x2000)
#define CS89xRxEventExtraData	(0x4000)

#define CS89xRxEventAltIAHash		(0x40)
#define CS89xRxEventAltDribbleBits	(0x80)
#define CS89xRxEventAltRxOK			(0x100)
#define CS89xRxEventAltHashed		(0x200)

#define CS89xTxEventLossOfCRS	(0x40)
#define CS89xTxEventSQEError	(0x80)
#define CS89xTxEventTxOK		(0x100)
#define CS89xTxEventOutOfWindow	(0x200)
#define CS89xTxEventJabber		(0x400)
#define CS89xTxEvent16Coll		(0x8000)

#define CS89xBufEventSWInt		(0x40)
#define CS89xBufEventRxDMAFrame	(0x80)
#define CS89xBufEventRdy4Tx		(0x100)
#define CS89xBufEventTxUnderrun	(0x200)
#define CS89xBufEventRxMiss		(0x400)
#define CS89xBufEventRx128		(0x800)
#define CS89xBufEventRxDest		(0x8000)

#define CS89xLineSTLinkOK		(0x80)
#define CS89xLineSTAUI			(0x100)
#define CS89xLineST10BT			(0x200)
#define CS89xLineSTPolarityOK	(0x1000)
#define CS89xLineSTCRS			(0x4000)

#define CS89xSelfST33VActive	(0x40)
#define CS89xSelfSTINITD		(0x80)
#define CS89xSelfSTSIBUSY		(0x100)
#define CS89xSelfSTEEPROMPres	(0x200)
#define CS89xSelfSTEEPROMOK		(0x400)
#define CS89xSelfSTELPresent	(0x800)
#define CS89xSelfSTEESize		(0x1000)

#define CS89xBusSTTxBidErr		(0x80)
#define CS89xBusSTRdy4TxNOW		(0x100)

#define CS89xEISACODE			(0x630E)

/*----------*/
/* Routines */
/*----------*/

int cs89xReset(void);
int cs89xSetup(void);
int cs89xStatus(void);
int cs89xSend(void *, unsigned short);
void cs89xISR(void);

#endif
